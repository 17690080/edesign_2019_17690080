/*
 * user.c
 *
 *  Created on: 25 Mar 2019
 *      Author: AZBoer
 */

#include "user.h"

volatile bool rxed;
volatile bool validchecksum = false;
volatile bool setburnpinto = false;
volatile bool shouldfall = false;
volatile bool GPGGAreceived = false;
char rxchar;
int runtime = 0;
#define CMDBUFLEN 80 //75
#define MAXTXLEN  90
#define RMS_WINDOW 20
char cmdbuffer[CMDBUFLEN];
char txbuffer[MAXTXLEN];
uint16_t cmdbufpos = 0;
char check1[2];
char check2[2];
int checksum = 0;
int stringlength = 0;
int sign1 = 1;
int sign2 = 1;
int h[2];
int m[2];
int s[2];
int nulcount1 = 0;
int nulcount2 = 0;
char londirect;
char latdirect;
int hours1   = 0;
int hours2   = 0;
int minutes1 = 0;
int minutes2 = 0;
int seconds1 = 0;
int seconds2 = 0;
int commacounter = 0;
char lat[10]; int intlat[8]; float flat = 0;
char lon[11] = "0.0"; int intlon[9]; float flon = 0;
char alt[7];
char latitude[12] = "0.0";
char longitude[13] = "0.0";
char altitude[7] = "0.0";
char gps[6];
char gpscheck[6] = "$GPGGA";
int GPGGAcheck = 0;
float falt = 0;
int setburncount = 0;
int burntime = 1;
int currenttime = 0;
int currenttime1 = 0;
int currentalt = 0;
int checkifpinsetburnworked = 0;
int checksum1;
char cchecksum1;
int intchecksum2;
int intchecksum1;
int i = 0;
int c = 0;

uint8_t adcchan;
uint32_t isample[RMS_WINDOW];
uint32_t vsample[RMS_WINDOW];
uint8_t samplectr;

float fvoltage = 0;
float fcurrent = 0;
float bcurrent = 0;
int ccurrent = 0;
char current1[5] = "0";
char current2[5] = "0.0";
char voltage[5] = "0.0";

int temp = 0;
int pres = 0;
int humi = 0;
char temperature[5] = "0";
char pressure[5] = "0";
char humidity[5] = "0";

int intalt2;

uint8_t bme280_id = 0x76;
struct bme280_dev dev;
uint8_t settings_sel;
struct bme280_data comp_data;

uint8_t lis2dh12_id = 0x18;

#define SENSOR_BUS hi2c1;

static axis3bit16_t data_raw_acceleration;
int16_t acceleration_mg[3];
lis2dh12_ctx_t dev_ctx;
lis2dh12_reg_t reg;
float temx;

int stnd_x;
int stnd_y;
int stnd_z;

double magn;
char accx[5] = "0";
char accy[5] = "0";
char accz[5] = "0";

void UserMainInit(void)
{
	HAL_UART_Receive_IT(&huart1, (uint8_t*)&rxchar, 1);
	sensor_init();
}


void UserMainLoopUpdate(void)
{
	runtime++;
	snprintf(current1, 4, "%d", ccurrent);
	snprintf(voltage, 4, "%f", fvoltage);

	temp = comp_data.temperature;
	pres = comp_data.pressure;
	humi = comp_data.humidity;

	stream_sensor_data_forced_mode(&dev);

	snprintf(temperature, 4, "%d", temp);
	snprintf(pressure, 4, "%d", pres);
	snprintf(humidity, 4, "%d", humi);

	stream_accelerometer_data();
	standardize_accel();
	Format_Accel();

	snprintf(accx, 5, "%d", stnd_x);
	snprintf(accy, 5, "%d", stnd_y);
	snprintf(accz, 5, "%d", stnd_z);

	uint8_t numcharswritten = 0;
	sprintf(txbuffer, "$17690080,%5d,%d%d:%d%d:%d%d,%3s,%3s,%3s,%4s,%4s,%4s,%10s,%11s,%7s,%3s,%3s\n",
			runtime, hours1, hours2, minutes1, minutes2, seconds1, seconds2, temperature, humidity, pressure, accx, accy, accz,latitude, longitude, altitude, current1, voltage);
	numcharswritten += strlen(txbuffer);
	HAL_UART_Transmit(&huart1, (uint8_t*)txbuffer, numcharswritten, 100);


	// Burn signal code
	//wait for 5 valid "out of bound" messages
	if(setburnpinto == true){
		setburnpinto = false;
		if(setburncount < 5){
			setburncount++;
		}
	}else{
		setburncount = 0;
	}

	// if 5 valid "out of bound" messages was receive activate burn signal
	if((setburncount == 5) && (currenttime == 0)){
		pin = true;
		currenttime = runtime;
	}

	// if 10 seconds past deactivate burn signal
	if((runtime - currenttime) == 10){
		pin = false;
		currenttime = 0;
		setburncount = 0;
		checkifpinsetburnworked++;
		currenttime1 = runtime;
	}

	writeAltBurnTempToLCD(intalt2, pin, temp);

	HAL_UART_Receive_IT(&huart1, (uint8_t*)&rxchar, 1);

}

void TestApp_String_Input(void){

	if ((cmdbuffer[1] == 'G') && (cmdbuffer[2] == 'P') && (cmdbuffer[3] == 'G') && (cmdbuffer[4] == 'G') && (cmdbuffer[5] == 'A') ) {
		GPGGAreceived = true;
	}else{
		GPGGAreceived = false;
	}

	if (GPGGAreceived) {

	//Checksum calculation
		for (intchecksum1 = 0, i = 0; i < strlen(cmdbuffer); i++) {
			c = (unsigned char)cmdbuffer[i];
			if (c == '*'){
				break;
			}
			if (c != '$'){
				intchecksum1 ^= c;
			}
		}

		char* message = cmdbuffer;
		char* Checksum = strchr(message, '*');
		char* checksum = strtok(Checksum, "*");

		intchecksum2 = strtol(checksum, NULL, 16);

		if(intchecksum2 == intchecksum1){
			validchecksum = true;
		}


     // Prepare output variables (gps time,latitude, longitude, and altitude) for output string --> sprintf in UserMainLoopUpdate if the checsum is true
	if(validchecksum){
	// extracts the GPS time from the cmdbuffer
		for(int k = 0; k < 8; k++){
			if(cmdbuffer[k] == ','){
				k++; h[0] = cmdbuffer[k] - '0';
				k++; h[1] = cmdbuffer[k] - '0';
				hours1 = h[0]; hours2 = h[1];
				k++; m[0] = cmdbuffer[k] - '0';
				k++; m[1] = cmdbuffer[k] - '0';
				minutes1 = m[0]; minutes2 = m[1];
				k++; s[0] = cmdbuffer[k] - '0';
				k++;
				if(cmdbuffer[k] == '.'){
					s[1] = seconds2;
				}else{
					s[1] = cmdbuffer[k] - '0';
				}
				seconds1 = s[0]; seconds2 = s[1];
				k = 8;
		}}

		for(int l = 0; l < strlen(cmdbuffer); l++){

			if(cmdbuffer[l] == ','){
				commacounter++;
			}

			// extract latitude from 'cmdbuffer'
			if(commacounter == 2){
				if(cmdbuffer[l+1] != ','){
					for(int a = 0; a < 10; a++){
						l++; lat[a] = cmdbuffer[l];
						if(cmdbuffer[l+1] == ','){
							latdirect = cmdbuffer[l+2];
							a = 10;
			}}}}

			//extract longitude from 'cmdbuffer'
			if(commacounter == 4){
				if(cmdbuffer[l+1] != ','){
					for(int b = 0; b < 11; b++){
						l++; lon[b] = cmdbuffer[l];
						if(cmdbuffer[l+1] == ','){
							londirect = cmdbuffer[l+2];
							b = 11;
			}}}}

			//extract altitude from 'cmdbuffer'
			if(commacounter == 9){
				if(cmdbuffer[l+1] != ','){
					for(int c = 0; c < 8; c++){
						l++; alt[c] = cmdbuffer[l];
						if(cmdbuffer[l+1] == ','){
							c = 7;
			}}}}


		}



		// checks is latitude is North or South and is then later used to change the latitude dd.dddddd to negative if South is detected
		int x = 0;
		int sign1 = 1;
		for(int m = 0; m < 10; m++){

			if(latdirect == 'S'){
				sign1 = -1;
			}

			if(lat[m] == '.'){
				m++;
			}

			intlat[x] = lat[m] - '0';
			x++;
		}


		// does gps latitude conversion from dddmm.mmmm to dd.dddddd and stores it in a float flat which is converted to an array latitude
		if(sign1 == -1){
			flat = (float)(sign1)*(intlat[0]*10 + intlat[1] + intlat[2]*10/(float)(60)
					+ intlat[3]/(float)(60) + intlat[4]/(float)(60*10)
					+ intlat[5]/(float)(60*100) + intlat[6]/(float)(60*1000)
					+ intlat[7]/(float)(60*10000));
			snprintf(latitude, 11, "%f", flat);
		}else{
			flat = (float)(intlat[0]*10 + intlat[1] + intlat[2]*10/(float)(60)
				   + intlat[3]/(float)(60) + intlat[4]/(float)(60*10)
				   + intlat[5]/(float)(60*100) + intlat[6]/(float)(60*1000)
				   + intlat[7]/(float)(60*10000));
			snprintf(latitude, 11, "%f", flat);
		}

		// checks is longitude is West or East and is then later used to change the longitude dd.dddddd to negative if West is detected
		int y = 0;
		int sign2 = 1;
		for(int n = 0; n < 10; n++){

			if(londirect == 'W'){
				sign2 = -1;
			}

			if(lon[n] == '.'){
				n++;
			}

			intlon[y] = lon[n] - '0';
			y++;

		}

	    // does gps longitude conversion from ddmm.mmmm to dd.dddddd and stores it in a float flon which is converted to an array longitude
		if(sign2 == -1){
			flon = (float)(sign2)*(intlon[0]*100 + intlon[1]*10 + intlon[2]
				    + intlon[3]*10/(float)(60) + intlon[4]/(float)(60)
					+ intlon[5]/(float)(60*10) + intlon[6]/(float)(60*100)
					+ intlon[7]/(float)(60*1000) + intlon[8]/(float)(60*10000));
			snprintf(longitude, 12, "%f", flon);
		}else{
			flon = (float)(intlon[0]*100 + intlon[1]*10 + intlon[2]
					+ intlon[3]*10/(float)(60) + intlon[4]/(float)(60)
				    + intlon[5]/(float)(60*10) + intlon[6]/(float)(60*100)
					+ intlon[7]/(float)(60*1000) + intlon[8]/(float)(60*10000));
			snprintf(longitude, 12, "%f", flon);
		}

		// changes altitude from 12568.00 to 12568.0 (it removes extra zero in the decimals)
		for(int o = 0; o < 8; o++){

			if(alt[o] == '.'){
				altitude[o] = alt[o];
				o++;
				altitude[o] = alt[o];
				o++;
				if(alt[o] == '0'){
					altitude[o] = '\0'; o++;
				}
			}else{
				altitude[o] = alt[o];
			}
		}

		int intalt = 0;

		for(int t = 0; t < strlen(altitude); t++){
			intalt *= 10;
	        intalt += (altitude[t] - '0');
	        if(altitude[t + 1] == '.'){
	        	t = strlen(altitude);
	        }
		}

		intalt2 = intalt;

		falt = 0;
		falt = (float)(intalt/(float)(1000));

		if(((flon < 17.976343) && (falt > 10)) || ((flon > 18.9354) && (falt > 10))){
			setburnpinto = true;
		}

		validchecksum = false;
		GPGGAreceived = false;
		commacounter = 0;
	}else{
		if(((flon < 17.976343) && (falt > 10)) || ((flon > 18.9354) && (falt > 10))){
			setburnpinto = true;
		}
	}
	}
	HAL_UART_Receive_IT(&huart1, (uint8_t*)&rxchar, 1);

}

// HAL_UART_RxCpltCallback - callback function that will be called from the UART interrupt handler. This function will execute whenever a character is received from the UART
void HAL_UART_RxCpltCallback(UART_HandleTypeDef* huart)
{
	//Stores NMEA GPGGA message in cmdbuffer
	if (cmdbufpos < CMDBUFLEN){
		if(rxchar == '$'){
			cmdbufpos = 0;
		}

		cmdbuffer[cmdbufpos] = rxchar;

		cmdbufpos++;
	}else{
		cmdbufpos = 0;
	}

	// check if a complete message has been send
	if(cmdbuffer[0] == 0x24 && cmdbuffer[cmdbufpos-2] == 0xD && cmdbuffer[cmdbufpos-1] == 0xA)
	{
		TestApp_String_Input();
	}

	HAL_UART_Receive_IT(&huart1, (uint8_t*)&rxchar, 1);
}


void ADC_Function(void){
	//ADC Code
	//if(adctick){

	if (adcchan == 0)
	{
		isample[samplectr] = HAL_ADC_GetValue(&hadc1);
	}
	else 
    if (adcchan == 1){
		vsample[samplectr] = HAL_ADC_GetValue(&hadc1);
	}
	
	adcchan++;
	
	if(adcchan > 1){

		adcchan = 0;
		samplectr++;

		if (samplectr >= RMS_WINDOW)
		{
			samplectr = 0;
		}
	}

		ADC_ChannelConfTypeDef chdef;

		switch (adcchan)
		{
			case 0: chdef.Channel = ADC_CHANNEL_1; break; //current
			case 1: chdef.Channel = ADC_CHANNEL_9; break; //voltage
		}

		chdef.Rank = 1;
		chdef.SingleDiff = ADC_SINGLE_ENDED;
		chdef.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
		chdef.OffsetNumber = ADC_OFFSET_NONE;
		chdef.Offset = 0;
		HAL_ADC_ConfigChannel(&hadc1, &chdef);

		HAL_ADC_Start(&hadc1);

		ADC_Calculate();

		adctick = 0;


}

void ADC_Calculate(void){

	int idigital = 0;
	int vdigital = 0;
	int idigitave = 0;
	int vdigitave = 0;

	for(int s = 0; s < 20; s++){
		idigital += isample[s];
		vdigital += vsample[s];
	}

	idigitave = idigital/20;
	vdigitave = vdigital/20;

	fcurrent = (((idigitave*(3.3/4095))/7.8))*1000;
	bcurrent = (float)(fcurrent)*(0.29);
	ccurrent = bcurrent;
	fvoltage = (vdigitave*(3.3/4095))*(3.34);
}


void sensor_init(void)
{
//	DevAddress Target device address: The device 7 bits address value in datasheet must be shifted to the left before calling the interface
	if(HAL_I2C_IsDeviceReady(&hi2c1, (bme280_id<<1)&0xFE, 2, 100) == HAL_OK)
	{

	    dev.dev_id = BME280_I2C_ADDR_PRIM; //Address = 0X76 (SDO -> GND)
	    dev.intf = BME280_I2C_INTF; //Set I2C Interface
	    dev.read = user_i2c_read; //READ Function
	    dev.write = user_i2c_write; //WRITE Function
	    dev.delay_ms = HAL_Delay; //DELAY Function

	    bme280_init(&dev);

	    /* Recommended mode of operation: Indoor navigation */
		dev.settings.osr_h = BME280_OVERSAMPLING_1X;
		dev.settings.osr_p = BME280_OVERSAMPLING_16X;
		dev.settings.osr_t = BME280_OVERSAMPLING_2X;
		dev.settings.filter = BME280_FILTER_COEFF_OFF;
		dev.settings.standby_time = BME280_STANDBY_TIME_62_5_MS;//BME280_STANDBY_TIME_500_MS;

		stream_sensor_data_forced_mode(&dev);
	}

	if(HAL_I2C_IsDeviceReady(&hi2c1, (lis2dh12_id<<1)&0xFE, 2, 100) == HAL_OK)
	{

		dev_ctx.write_reg = platform_write;
		dev_ctx.read_reg = platform_read;
		dev_ctx.handle = &hi2c1;

		lis2dh12_block_data_update_set(&dev_ctx, PROPERTY_ENABLE);
		lis2dh12_data_rate_set(&dev_ctx, LIS2DH12_ODR_10Hz);
		lis2dh12_full_scale_set(&dev_ctx, LIS2DH12_2g);
		lis2dh12_temperature_meas_set(&dev_ctx, LIS2DH12_TEMP_DISABLE);
		lis2dh12_operating_mode_set(&dev_ctx, LIS2DH12_NM_10bit);

	}

}

void stream_accelerometer_data(void)
{

  /*
   * Read samples in polling mode (no int)
   * Read output only if new value available
   */

  lis2dh12_xl_data_ready_get(&dev_ctx, &reg.byte);

  if (reg.byte)
  {
	  /* Read accelerometer data */
	  memset(data_raw_acceleration.u8bit, 0x00, 3*sizeof(int16_t));
	  lis2dh12_acceleration_raw_get(&dev_ctx, data_raw_acceleration.u8bit);

	  acceleration_mg[0] = lis2dh12_from_fs2_hr_to_mg(data_raw_acceleration.i16bit[0]);

	  acceleration_mg[1] = lis2dh12_from_fs2_hr_to_mg(data_raw_acceleration.i16bit[1]);

	  acceleration_mg[2] = lis2dh12_from_fs2_hr_to_mg(data_raw_acceleration.i16bit[2]);
  }
}

void standardize_accel(void)
{
	//++stndz;
	magn = pow(acceleration_mg[0], 2) + pow(acceleration_mg[1], 2) + pow(acceleration_mg[2], 2);
	magn = pow(magn, 0.5);

	stnd_x = round(-acceleration_mg[2]*100/magn)*10;
	stnd_y = round(acceleration_mg[1]*100/magn)*10;
	stnd_z = round(-acceleration_mg[0]*100/magn)*10;

}


void stream_sensor_data_forced_mode(struct bme280_dev *dev)
{
    settings_sel = BME280_OSR_PRESS_SEL | BME280_OSR_TEMP_SEL | BME280_OSR_HUM_SEL | BME280_FILTER_SEL;
    bme280_set_sensor_settings(settings_sel, dev);
    bme280_set_sensor_mode(BME280_FORCED_MODE, dev);
	dev->delay_ms(40);
	bme280_get_sensor_data(BME280_ALL, &comp_data, dev);
}

int8_t user_i2c_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len)
{
    int8_t rslt = 0; /* Return 0 for Success, non-zero for failure */

    /*
     * HAL_I2C_Master_Transmit(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData, uint16_t Size, uint32_t Timeout)
     * HAL_I2C_Master_Receive(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint8_t *pData, uint16_t Size, uint32_t Timeout)
     * The parameter dev_id can be used as a variable to store the I2C address of the device
     */

    /*
     * Data on the bus should be like
     * |------------+---------------------|
     * | I2C action | Data                |
     * |------------+---------------------|
     * | Start      | -                   |
     * | Write      | (reg_addr)          |
     * | Stop       | -                   |
     * | Start      | -                   |
     * | Read       | (reg_data[0])       |
     * | Read       | (....)              |
     * | Read       | (reg_data[len - 1]) |
     * | Stop       | -                   |
     * |------------+---------------------|
     */

    HAL_I2C_Master_Transmit(&hi2c1, (dev_id<<1)&0xFE, &reg_addr, 1, 100);
    HAL_I2C_Master_Receive(&hi2c1, ( ((dev_id<<1)&0xFE) + 1 ), reg_data, len, 100);

    return rslt;
}

int8_t user_i2c_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len)
{
	int8_t rslt = 0; /* Return 0 for Success, non-zero for failure */

	/*
     * The parameter dev_id can be used as a variable to store the I2C address of the device
     */

    /*
     * Data on the bus should be like
     * |------------+---------------------|
     * | I2C action | Data                |
     * |------------+---------------------|
     * | Start      | -                   |
     * | Write      | (reg_addr)          |
     * | Write      | (reg_data[0])       |
     * | Write      | (....)              |
     * | Write      | (reg_data[len - 1]) |
     * | Stop       | -                   |
     * |------------+---------------------|
     */

/*

    int8_t *i2c_buff;
    i2c_buff = malloc(len +1);
    i2c_buff[0] = reg_addr;
    memcpy(i2c_buff+1, reg_data, len);

    HAL_I2C_Master_Transmit(&hi2c1, (dev_id<<1)&0b11111110, (uint8_t*)i2c_buff, len+1, 100);
    free(i2c_buff);
*/

	HAL_I2C_Mem_Write(&hi2c1, (dev_id<<1)&0b11111110, reg_addr, 1, reg_data, len, 100);

    return rslt;
}

//void user_delay(uint32_t delay){
//
//	DWT_Delay(delay*1000);
//}

int32_t platform_write(void *handle, uint8_t reg, uint8_t *bufp,uint16_t len)
{
  if (handle == &hi2c1)
  {
    /* Write multiple command */
    reg |= 0x80;
    HAL_I2C_Mem_Write(handle, LIS2DH12_I2C_ADD_L, reg,
                      I2C_MEMADD_SIZE_8BIT, bufp, len, 1000);
  }
  return 0;
}

int32_t platform_read(void *handle, uint8_t reg, uint8_t *bufp, uint16_t len)
{
  if (handle == &hi2c1)
  {
    /* Read multiple command */
    reg |= 0x80;
    HAL_I2C_Mem_Read(handle, LIS2DH12_I2C_ADD_L, reg, I2C_MEMADD_SIZE_8BIT, bufp, len, 1000);
  }
  return 0;
}

void Format_Accel(void)
{

	stnd_x = stnd_x - 40;
	stnd_y = stnd_y + 20;


	if(stnd_x >= 1000)
	{
		stnd_x = 999;
	}

	if(stnd_y >= 1000)
	{
		stnd_y = 999;
	}

	if(stnd_z >= 1000)
	{
		stnd_z = 999;
	}

	if(stnd_x <= -1000)
	{
		stnd_x = -999;
	}

	if(stnd_y <= -1000)
	{
		stnd_y = -999;
	}

	if(stnd_z <= -1000)
	{
		stnd_z = -999;
	}

}


////true stuff
