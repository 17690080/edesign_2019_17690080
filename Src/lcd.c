/*
 * lcd.c
 *
 *  Created on: 05 Feb 2016
 *      Author: jtreurn
 */

#include "lcd.h" // or any other includes that makes sense
#include "main.h"
#include <stdbool.h>
//#include "r_cg_userdefine.h"
//#include "globals.c"
// Notes:
// This is a basic low-level set of functions to drive the LCD. The functions include
// 	- void initLcd(void) --- set up the LCD; requires a function DWT_Delay(time in usec) to operate
// 	- void writeNibbleLcd(uint8_t reg, uint8_t nibble) --- low level command
// 	- void writeByteLcd(uint8_t reg, uint8_t value) --- lowest user level command
//
// To be useful, you should write your own functions to:
//	- write an ASCII character to the LCD
//	- write a whole string to the LCD
//	- write a delay function DWT_Delay(delay in usec)

/************************************************************************
 * Function Name: writByteLcd()
 * Description  : This function writes 2 nibbles to the LCD.
 * Arguments    :  reg (BOOL) - 0 = register, 1 = data
 *              :  value (BYTE) - first the upper 4 bits and the 4 lower bits
 * Actions      : Nibble bus P70-P73 are set in output mode
 *              : LCD_RS = 1; LCD_RW -> 0; LCD_E is toggled
 * Return Value : None
 ************************************************************************/

void writeByteLcd(uint8_t reg, uint8_t value)
{
//	uint8_t tempReg;
//
//	tempReg = value >> 4;			// Get high nibble
//	writeNibbleLcd(reg, tempReg);	// Write high nibble
//
//	tempReg = value & 0x0F;			// Get lower nibble
//	writeNibbleLcd(reg, tempReg);	// Write lower nibble
}

/************************************************************************
 * Function Name: writeNibbleLcd()
 * Description  : This function writes a nibble to the LCD.
 * Arguments    : value (BYTE) - only the lower 4 bits are used
 * Actions      : Nibble bus P70-P73 set in output mode
 *              : LCD_RS = 1; LCD_RW -> 0; LCD_E is toggled
 * Return Value : None
 ************************************************************************/

void writeNibbleLcd(int regOrCommand, uint8_t nibble)
{
//
}

void LCD_EnablePulse(void)
{
	HAL_GPIO_WritePin(GPIOB, LCD_E, 1U);
	DWT_Delay(10);
	HAL_GPIO_WritePin(GPIOB, LCD_E, 0U);
	DWT_Delay(60);
}

void LCD_RS_State(bool state)
{
	if(state) HAL_GPIO_WritePin(GPIOA, LCD_RS, 1U);
	else HAL_GPIO_WritePin(GPIOA, LCD_RS, 0U);
}

void write4BitCommand(uint8_t nibble)
{
	uint8_t LSB_nibble = nibble&0xF;
	//Set RS to 0
	LCD_RS_State(false);
	//LSB data
	HAL_GPIO_WritePin(GPIOB, LCD_D4, (GPIO_PinState)(LSB_nibble&0x1));
	HAL_GPIO_WritePin(GPIOB, LCD_D5, (GPIO_PinState)(LSB_nibble&0x2));
	HAL_GPIO_WritePin(GPIOC, LCD_D6, (GPIO_PinState)(LSB_nibble&0x4));
	HAL_GPIO_WritePin(GPIOA, LCD_D7, (GPIO_PinState)(LSB_nibble&0x8));
	//Write the Enable pulse
	LCD_EnablePulse();
}

void LCD_write_Command(uint8_t command)
{
	//Set RS to 0
	LCD_RS_State(false);
	//Call low level write parallel function
	LCD_write(command);
}

void LCD_write_Data(uint8_t data)
{
	//Set RS to 1
	LCD_RS_State(true);
	//Call low level write parallel function
	LCD_write(data);
}

void LCD_write(uint8_t byte)
{
	uint8_t LSB_nibble = byte&0xF, MSB_nibble = (byte>>4)&0xF;

	//write data to output pins
	//MSB data
	HAL_GPIO_WritePin(GPIOB, LCD_D4, (GPIO_PinState)(MSB_nibble&0x1));
	HAL_GPIO_WritePin(GPIOB, LCD_D5, (GPIO_PinState)(MSB_nibble&0x2));
	HAL_GPIO_WritePin(GPIOC, LCD_D6, (GPIO_PinState)(MSB_nibble&0x4));
	HAL_GPIO_WritePin(GPIOA, LCD_D7, (GPIO_PinState)(MSB_nibble&0x8));
	//Write the Enable pulse
	LCD_EnablePulse();

	//LSB data
	HAL_GPIO_WritePin(GPIOB, LCD_D4, (GPIO_PinState)(LSB_nibble&0x1));
	HAL_GPIO_WritePin(GPIOB, LCD_D5, (GPIO_PinState)(LSB_nibble&0x2));
	HAL_GPIO_WritePin(GPIOC, LCD_D6, (GPIO_PinState)(LSB_nibble&0x4));
	HAL_GPIO_WritePin(GPIOA, LCD_D7, (GPIO_PinState)(LSB_nibble&0x8));
	//Write the Enable pulse
	LCD_EnablePulse();
}
/************************************************************************
 * Function Name: initLcd()
 * Description  : This function initializes the LCD.
 * Arguments    :  none
 * Actions      : Provide all the init instructions - clear screen
 * Return Value : None
 ************************************************************************/

void initLcd(void)
{
//	TMMK00 = 1U;					// Do not use interrupts here, just wait for the flags

	DWT_Delay(15000);				// wait 15 msec for LCD power up

	write4BitCommand(0x3); 	// Command 1 -> 8 bits
	DWT_Delay(4100);				// Start timer with 4100 usec delay

	write4BitCommand(0x3);	// Command 2 -> 8 bits
	DWT_Delay(100);				// Start timer with 100 usec delay

	write4BitCommand(0x3);	// Command 3 -> 8 bits
	DWT_Delay(40);					// Start timer with 40 usec delay

	write4BitCommand(0x2);	// Command 4 -> 4 bits
	DWT_Delay(40);					// Start timer with 40 usec delay

	LCD_write_Command(FUNCTION_SET);  // Set 2 lines, 5x7 dots
	DWT_Delay(40);					// Start timer with 40 usec delay

	LCD_write_Command(LCD_CURSOR_ON);     // Display Cursor ON
	DWT_Delay(40);					// Start timer with 40 usec delay

	LCD_write_Command(LCD_CLEAR);          // Clear Display
	DWT_Delay(1640);				// Start timer with 1640 usec delay

	LCD_write_Command(CURSOR_MODE_INC);    // Entry Mode set
	DWT_Delay(40);					// Start timer with 40 usec delay

	LCD_write_Command(LCD_HOME_L1);
	DWT_Delay(40);					// Start timer with 40 usec delay
}

void clearDisplay()
{
	LCD_write_Command(LCD_CLEAR);
	DWT_Delay(1650);
}

void writeAltBurnTempToLCD(int altitude, bool burn, int temperature)
{
	char message[16] = "                ";
	char alt[6] = {'\0'};
	sprintf(alt,"%dm",altitude);
	for(int i = 0; i < 6; i++)
	{
		if(alt[i] == '\0')
		{
			continue;
		}
		message[i] = alt[i];
	}
	if(burn)
	{
		message[9] = 'B';
	}

	char temp[4] = {'\0'};
	sprintf(temp,"%dC",temperature);
	int counter = 1;
	for(int i = 3; i >= 0; i--)
	{
		if(temp[i] == '\0')
		{
			continue;
		}
		message[16-counter] = temp[i];
		counter++;
	}
	clearDisplay();
	writeStringLcd(message);
}

void writeStringLcd(char message[])
{

int i = 0;

	while(message[i] != '\0')
	{
		if(i == 8)
		{
			LCD_write_Command(LCD_HOME_L2);
			DWT_Delay(40);
		}

		LCD_write_Data(message[i]);
		DWT_Delay(40);
		i++;
	}
}
//add
void printLcd(uint8_t *charArray, int size)
{
	int j;
	int k;
	for (j = 0; j < size; j++)
	{

		writeByteLcd(1U, charArray[j]);
		DWT_Delay(40);
		if(j == 7){
			for(k = 0; k<32;k++){
				writeByteLcd(0U, LCD_CURSOR_RIGHT);
				DWT_Delay(40);
			}
		}

	}

}


//ADD
void scrollLcd(uint8_t *message){

	int x;
	int y = 0;
	int k;

	for(x = 0; message[x]!= 0xD;x++){
		writeByteLcd(1U, message[x]);
		DWT_Delay(40);

		if(y == 15){
			DWT_Delay(16000);
			DWT_Delay(16000);
			DWT_Delay(16000);
			DWT_Delay(16000);
			DWT_Delay(16000);
			DWT_Delay(16000);
			DWT_Delay(16000);
			DWT_Delay(16000);
			DWT_Delay(16000);
			DWT_Delay(16000);
			DWT_Delay(16000);
			DWT_Delay(16000);
			DWT_Delay(16000);
			DWT_Delay(16000);
			DWT_Delay(16000);
			DWT_Delay(16000);
			DWT_Delay(16000);
			writeByteLcd(0U, LCD_CLEAR);
			DWT_Delay(1640);
			x = x-15;
			y=-1;
		}
		if(y==7){
			for(k = 0; k<32;k++){
				writeByteLcd(0U, LCD_CURSOR_RIGHT);
				DWT_Delay(40);
			}
		}
		y = y+1;
	}

}




