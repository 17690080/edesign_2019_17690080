/*
 * lcd.h
 *
 *  Created on: 24 Mar 2015
 *      Author: jtreurn
 */

#ifndef LCD_H_
#define LCD_H_

#include <stdint.h>
#include <stdbool.h>
#include "dwt_delay.h"

	// Local LCD Port 7 bit allocation
#define LCD_RS              GPIO_PIN_8		// Register select
#define LCD_RW              GPIO_PIN_4		// Read/Write line
#define LCD_E               GPIO_PIN_5		// Enable Strobe
#define LCD_D4              GPIO_PIN_3		// 4 Nibble data lines
#define LCD_D5              GPIO_PIN_6
#define LCD_D6              GPIO_PIN_7
#define LCD_D7              GPIO_PIN_9
//
//#define Load_Enable         P4_bit.no1
//#define Motor_Enable        P4_bit.no2
//#define Short_Enable        P4_bit.no3
//#define OKbutton                  P5_bit.no3
//#define downButton                  P5_bit.no4
//#define upButton                  P5_bit.no5
//#define cancelButton                 P5_bit.no2
//#define temp                P1_bit.no7


	// Other LCD defines
#define LCD_DATA_WR 		1				// Data write
#define LCD_CTRL_WR 		0				// Command
#define LCD_LINE1 			0				// Line 1 offset
#define LCD_LINE2 			16				// Line 1 offset

	// LCD Commands
#define LCD_CLEAR        	0x01			// Clear LCD display and home cursor
#define LCD_HOME_L1      	0x80			// Move cursor to line 1
#define LCD_HOME_L2      	0xC0			// Move cursor to line 2
#define CURSOR_MODE_DEC  	0x04			// Cursor auto decrement after R/W
#define CURSOR_MODE_INC  	0x06			// Cursor auto increment after R/W
#define FUNCTION_SET     	0x28			// Setup, 4 bits,2 lines, 5X7
#define LCD_CURSOR_ON    	0x0E			// Display ON with Cursor
#define LCD_CURSOR_OFF   	0x0C			// Display ON with Cursor off
#define LCD_CURSOR_BLINK 	0x0D			// Display on with blinking cursor
#define LCD_CURSOR_LEFT  	0x10			// Move Cursor Left One Position
#define LCD_CURSOR_RIGHT 	0x14			// Move Cursor Right One Position
#define LCD_DISPLAY_ON   	0x04			// Turn display ON
#define LCD_TWO_LINE     	0x08			// Set cursor to start of line 2

	// Enable Strobe macros
//#define	SET_LCD_EN_HIGH		LCD_E = 1 		//asm("SET1 0xFFF0E.6")
//#define	SET_LCD_EN_LOW		LCD_E = 0 		//asm("CLR1 0xFFF0E.6")

//#include "r_cg_macrodriver.h"

	// Prototypes
void initLcd(void);
void writeNibbleLcd(int regOrCommand, uint8_t nibble);
void writeByteLcd(uint8_t reg, uint8_t value);
void scrollLcd(uint8_t *message);
void writeStringLcd(char message[]);
void printLcd(uint8_t *charArray, int size);
//void delayNoInt(uint16_t delay);
//void startTMR0(int delay);
//void decodeCmdUart1(uint8_t* cmdbuffer, uint8_t cmdlen);

void LCD_RS_State(bool state);
void LCD_EnablePulse(void);
void write4BitCommand(uint8_t nibble);
void LCD_write_Command(uint8_t command);
void LCD_write_Data(uint8_t data);
void LCD_write(uint8_t byte);
void writeAltBurnTempToLCD(int altitude, bool burn, int temp);
void clearDisplay();

void setRTC(uint8_t* setbuffer);
void getRTC(void);
void setDate(char hold[]);
void delay(void);
#endif /* LCD_H_ */

