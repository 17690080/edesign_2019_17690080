/*
 * user.h
 *
 *  Created on: 25 Mar 2019
 *      Author: AZBoer
 */

#ifndef USER_H_
#define USER_H_

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "main.h"
#include "lcd.h"
#include "stm32f3xx_hal.h"
#include "bme280.h"
#include "bme280_defs.h"
#include <time.h>
#include <lis2dh12_reg.h>


// extern variable declarations - tell the compiler that these variables exist, and are defined elsewhere
extern UART_HandleTypeDef huart1;

extern ADC_HandleTypeDef hadc1;

extern I2C_HandleTypeDef hi2c1;
// function prototypes

// UserMainInit - initialisation code, that should be executed just before the main loop starts
void UserMainInit(void);

// UserMainLoopUpdate - function that gets called from main, with our user code in it. This function does not in itself contain a loop, but is called with every iteration of the main loop.
void UserMainLoopUpdate(void);

void TestApp_String_Input(void);

void ADC_Function(void);

void ADC_Calculate(void);

void sensor_init(void);

void sensor_init(void);

void stream_sensor_data_forced_mode(struct bme280_dev *dev);

int8_t user_i2c_read(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len);

int8_t user_i2c_write(uint8_t dev_id, uint8_t reg_addr, uint8_t *reg_data, uint16_t len);

int32_t platform_read(void *handle, uint8_t reg, uint8_t *bufp, uint16_t len);

int32_t platform_write(void *handle, uint8_t reg, uint8_t *bufp,uint16_t len);

void stream_accelerometer_data(void);

void standardize_accel(void);

void user_delay(uint32_t delay);

void Format_Accel(void);

volatile bool pin;

volatile bool adctick;

#endif /* USER_H_ */
